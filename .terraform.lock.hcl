# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.7.0"
  hashes = [
    "h1:Xe8KHFOG3zA61RjvDtVgWKCPeT4l1++XEGVJyNEeeM4=",
    "zh:16addba6bda82e6689d21049392d296ce276cb93cbc5bcf3ad21f7d39cd820fd",
    "zh:1e9dd3db81a38d3112ddb24cec5461d919e7770a431f46ac390469e448716fd4",
    "zh:252c08d473d938c2da2711838db2166ecda2a117f64a10d279e935546ef7d991",
    "zh:2e0c83da0ba44e0521cb477dd60ea7c08b9edbc44b607e5ddb963becb26970a5",
    "zh:396223391782f1f809a60f045bfdcde41820d0d6119912718d86fc153fc28969",
    "zh:3a6b3c0901b81bc451d1ead2a2f26845d5db6b6253758c1f0aa0bad53fb6b4bd",
    "zh:51010e8f1d05f4979f0e10cf0e3b56cec13c731d99f373dda9fd9561ddb2826b",
    "zh:53ef55edf7698cbb4562265a6ab9e261716f62a82590e5fb6ad4f7f7458bdc5c",
    "zh:6c2db10e6226cc748e6dd5c1cbc257fda30cd58a175a32fc95a8ccd5cebdd3e7",
    "zh:91627f5af7e8315479a6c45cb1ae5db3c0a91a18018383cd409f3cfa04408aed",
    "zh:b5217a81cfc58334278831eacb2865bd8fc025b0cb1c576e9da9c4dc3a187ef5",
    "zh:c70afea4324518b099d23abc189dff22e6706ca0936de39eca01851e2550af7e",
    "zh:e62c212169ef9aad3b01f721db03b7e08d7d4acbbac67a713f06239a3a931834",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.54.0"
  constraints = ">= 3.15.0"
  hashes = [
    "h1:iyxZssXd79FWHVGsMd1MuTO63LUum6JuVE0eJBWTYEs=",
    "zh:06c2dfbbf8ca2bea50444c26c5159763fe9cbe92553c13a208b4df1035368f35",
    "zh:2ea54ab59a3a4cf455a1f1589778ca0721ed5d6ad681d64cfa7f2036dcc6df61",
    "zh:30719cb435dac60fa69564e2ce221d589be3a974daed966f88fae88782db3a67",
    "zh:8b13e240955bd9181dd5d0081bbc7c03200b4493e2e9f52a783c5e1582e16311",
    "zh:99b344ab400300309e27dea4195671d1b021dee44758d0a97236f171b8395cbf",
    "zh:bc0d72a343b161c3b47ce78760b9deae0d9339e279c3be35a2e9f148c09c4688",
    "zh:d905eaa3c074131d8e7e5207a3c9ddab73c32a792dcd97a1c9d52a2a9753c1b7",
    "zh:dddc2752b9d6846eaef3b2e2486f8789df91d16794e7aae03c9d5d6cb1bc10eb",
    "zh:ed1bee76d38117594f336bd2656135c7f85b4a1af3ff65d2a3e59a8107b9219a",
    "zh:f1f1a0bebd03e479b072dde9831f573070f6deada2972f5b966dd4858b99a8c2",
    "zh:f91a6dfbc46d523d5f90ae84ac6ee6bf0aa2b1ced9a55f6142669849d3608998",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.4.1"
  hashes = [
    "h1:wU6cDBN6KPhjbBvPWXRgryN9amNlhL/n9l39cFm3X/U=",
    "zh:10a368f3a3f26d821f02b55f0c42bdd4d2cd0dc5e2568c513bce39d92d25526f",
    "zh:2183272a6d44f23d562d47ff4d6592685d8797838bdae69a50f92121743b020f",
    "zh:24c492d61ce4dbcac4bb4410bd5e657ab28d19ab320d41104148ee626b44f5ed",
    "zh:291380db0cd581d806158e5ddfd7133592055151109fcf0c923644cede5f30c7",
    "zh:46933ddae44108d1a2956d917bafdb8879147b204b1bfac0c238773d2587e288",
    "zh:5b96c1c330d709d87faa44f1cc9b1db87baeba5056638fe07c51a9b5a67f297e",
    "zh:9fbb4ac6de96f68df324adbb77fd5eee6138f534f5393dc3bac18e615c75e0d0",
    "zh:b8da6bbb97c20ec6e26c0160060c24d4e91b5057342b8b93a43f4019ab36e344",
    "zh:c12390d668ef2f4c943c385de3befb54c0bfd0f9a3aa28b6aec55f7db4f4a518",
    "zh:dee3d13f664037ada51e6f51c7e1c1361e643e1e61fbc9403b0f3985caa29c90",
    "zh:ed10c04a636fa4a0f6e5e6068cb2f9a0f976b596cbabb9bd429631e3ba7fa35a",
  ]
}
