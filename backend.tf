terraform {
  backend "remote" {
    organization = "gitops-training"

    workspaces {
      name = "aws"
    }
  }
}