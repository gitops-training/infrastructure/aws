/*
* VCP AWS INFRASTRUCTURE
* v0.1
*/
provider "aws" {
  region = "sa-east-1"
}

locals {
  name   = "gitops"
  region = "sa-east-1"
  tags = {
    Owner       = "py-paulo"
    Environment = "prd"
    Name        = "gitops",
    Terraform   = "true"
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = local.name
  cidr = "10.0.0.0/16"

  azs             = ["${local.region}a", "${local.region}b", "${local.region}c"]
  #
  # https://www.site24x7.com/pt/tools/ipv4-sub-rede-calculadora.html
  #
  # Bloqueio de endereço de rede:   10.0.0.0/16
  # Máscara de sub-rede:            255.255.255.0/24
  # Número de hosts/sub-redes:      256
  # Número de sub-redes:            256
  # Intervalo de endereço de host:  10.0.0.1 - 10.0.0.254
  # Máscara curinga:                0.0.0.255
  # Notações de CIDR:               10.0.0.0/24
  private_subnets     = ["10.0.0.0/24",   "10.0.1.0/24",   "10.0.2.0/24"]
  public_subnets      = ["10.0.3.0/24",  "10.0.4.0/24",   "10.0.5.0/24"]
  intra_subnets       = ["10.0.6.0/24", "10.0.7.0/24",  "10.0.8.0/24"]
  database_subnets    = ["10.0.9.0/24", "10.0.10.0/24",  "10.0.11.0/24"]

  enable_nat_gateway                = true
  enable_vpn_gateway                = true
  enable_dns_hostnames              = true
  enable_dns_support                = true

  manage_default_security_group     = false
  single_nat_gateway                = false
  propagate_public_route_tables_vgw = true
  reuse_nat_ips                     = true
  one_nat_gateway_per_az            = true

  public_subnet_tags = {
    "kubernetes.io/cluster/cls_eks" = "shared"
    "kubernetes.io/role/elb"        = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/cls_eks"   = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }

  tags = local.tags

  vpc_tags = {
    Name      = "gitops",
    Version   = "0.1"
  }
}
