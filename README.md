# AWS infrastructure

Infraestrutura para [AWS](https://aws.amazon.com/pt/) escrita em [Terraform](https://www.terraform.io/). Esse projeto faz o gerenciamento de um ambiente completo na *aws*, inclui diferentes *subnets* distribuidas nas zonas de disponibilidade de uma área na *aws*.

Desenho da arquitetura:

![AWS](./assets/aws.png)

## VPC

A **vpc** tem o **cidr** (*Classes Inter-Domain Routing*) `10.0.0.0/16`, gerando até **256** *subnets*, cada com uma máscara `255.255.255.0/24`. São contempladas 4 subnets `private_subnet`, `public_subnets`, `intra_subnets`, `database_subnets`. 

**private_subnets**: ambiente interno de desenvolvimento.
**public_subnets**: para deploy de aplicações em produção.
**intra_subnets**: ambientes internos sem comunicação com a internet para garantir maior segurança em aplicações críticas.
**database_subnets**: rede separada para bancos de dados.

> Está habilitado apenas um *gateway* por zona.